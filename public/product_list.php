<?php include 'inc/header.php';
$item = new Item; // Initialize Item class, which deals with DB interactions
checkForm(); // Check if mass action button was used

/* Pagination setup */
$itemCount = $item->getAllItemCount()['count'];
$pageLimit = 12;
$pages = ceil($itemCount / $pageLimit);
$page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
    'options' => array(
        'default'   => 1,
        'min_range' => 1,
    ),
))); // Store current page as a var
$offset = ($page - 1) * $pageLimit;
$prevlink = ($page > 1) ? '<a href="product_list.php?page=1" title="First Page">&laquo;</a> <a href="product_list.php?page=' . ($page - 1) . '" title="Previous Page">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';
$nextlink = ($page < $pages) ? '<a href="product_list.php?page=' . ($page + 1) . '" title="Next page">&rsaquo;</a> <a href="product_list.php?page=' . $pages . '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';
/* Pagination setup ends here */
?>
    <div class="container">
        <!-- List Header Div -->
        <div class="row mb-3">
            <div class="col-md-12 px-1">
                <h3>Product List</h3>
                <div class="float-right">
                    <div id="list-form"> 
                    <!-- Form content inserted with JavaScript
                        Checkmark values inserted as hidden HTML input for PHP
                     -->
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <!-- Main Content -->
        <div class="store-items">
            <?php
            if ($itemCount < 1) {
                echo "Sorry, our store has sold out!";
            } else {
                $items = $item->getPageItems($pageLimit, $offset);
                $i = 1;
                while ($rows = $items->fetch_assoc()) {
                if ($i === 1 || $i % 4 ===1) {
                    echo '<div class="row">'; // Bootstrap row inserted for every 4 store items
                }    
                ?>
                    <!-- Col-md-3 inserted for every store item -->
                    <div class="col-md-3   mb-4 text-center">
                        <div class="card">
                            <form action="#">
                                <input type="checkbox" name="<?= $rows['id'] ?>" value="<?= $i ?>" class="item-checkbox mb-0">
                            </form>
                            <p><?= $rows['sku']; ?></p>
                            <p><?= $rows['name']; ?></p>
                            <p><?= $rows['price']; ?> $</p>
                            <!-- p value changed based on type -->
                            <p class="spec"><?php  switch ($rows['type']) {
                                case "cd":
                                    echo "Size: " . $rows['spec_value'] . " MB";
                                    break;
                                case "furniture":
                                    echo "Dimension: " . $rows['spec_value'];
                                    break;
                                case "book":
                                    echo "Weight: " . $rows['spec_value']. " KG";
                                    break;
                            } ?></p>
                        </div>
                    </div>
                <?php
                if ($i % 4 === 0 || $i === $itemCount % $pageLimit && $page == $pages) {
                    echo "</div>"; // Row ends after fourth col-md-3
                }
                $i++; // i keeps track of items for rows
                } ?>
                <!-- Pagination control -->
                <div class="row mb-3 d-block">
                    <div class="col-md-12 px-0">
                        <div class="pagination d-flex justify-content-center">
                            <?php echo $prevlink;
                            echo $nextlink; ?>
                        </div>
                        
                    </div>
                </div>
            <?php } ?>
            </div>    
        </div>
    </div>
<script src="public/inc/js/scripts_list.js"></script>
<?php include 'inc/footer.php'; ?>
