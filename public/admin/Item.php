<?php
require "admin/db.php";

class Item {

    private $database;
    public function __construct() {
        $this->database = new Database;
        
    }

    public function getAllItems() {
        $sql = "SELECT * FROM items";
        $results = $this->database->query($sql);
        if (!$results) {
            return false;
        }
        return $results;
    }

    public function getPageItems($limit, $offset) {
        $sql = "SELECT * FROM items ORDER BY id DESC LIMIT " . $limit . " OFFSET " . $offset;
        $results = $this->database->query($sql);
        if (!$results) {
            return false;
        }
        return $results;

    }

    public function getAllItemCount() {
        $sql = "SELECT COUNT(*) AS count FROM items";
        $results = $this->database->query($sql);
        $results = mysqli_fetch_assoc($results);
        if (!$results) {
            return false;
        }
        return $results;
    }

    public function deleteItems($sql) {
        $this->database->query($sql);
    }

    public function isSkuUnique($sku) {
        $sql = "SELECT sku FROM items WHERE sku = ?";
        $stmt = $this->database->prepare($sql);
        $stmt->bind_param("s", $sku);
        $stmt->execute();
        $stmt->bind_result($result);
        $stmt->fetch();
        if($result) {
            return false;
        } else {
            return true;
        }
    }

    public function insertCd($post) {
        // Call_user_array_func_only accepts references in array, so a new reference array is made for each type upon sumbission
        $postRefs = array(&$post[0], &$post[1], &$post[2], &$post[3], &$post[4]); 
        $stmt = $this->database->prepare_item();
        $this->database->bind_item_params($stmt, $postRefs);
    }

    public function insertFurniture($post) {
        // Stores 3 values in a single string
        $specificValue = $post[5] . "x" . $post[6] . "x" . $post[7];
        $postRefs = array(&$post[0], &$post[1], &$post[2], &$post[3], &$specificValue);
        $stmt = $this->database->prepare_item();
        $this->database->bind_item_params($stmt, $postRefs);
    }

    public function insertBook($post) {
        $postRefs = array(&$post[0], &$post[1], &$post[2], &$post[3], &$post[8]);
        $stmt = $this->database->prepare_item();
        $this->database->bind_item_params($stmt, $postRefs);
    }
}
