<?php
/* Check for delete submission on list page */
function checkForm() {
    if (isset($_POST['select'])) { // Is used when mass action button clicked
        if($_POST['select'] === 'delete') { // Delete option selected
            if ($_POST['check-input'] != null) {
                $toDelete = $_POST['check-input'];
                $toDeleteArray = explode(",", $toDelete);
                if(count($toDeleteArray) === 1) {
                $sql = "DELETE FROM items WHERE id = {$_POST['check-input']}";
                } elseif(count($toDeleteArray > 1)) { // Logic for deleting multiple items
                    $sql = "DELETE FROM items WHERE id IN (";
                    $i = 0;
                    foreach($toDeleteArray as $id) {
                        $sql = $sql . $id;
                        if ($i != (count($toDeleteArray) - 1)) {
                            $sql = $sql . ", ";
                        }
                        $i++;
                    }
                    $sql = $sql . ")";
                }
                $item = new Item;
                $item->deleteItems($sql);
            }
        }
    }
}

/*  Handle item submission */
function checkAdd() {
    if (!empty($_POST)) {
        $item = new Item;
        $_SESSION['selected_type'] = $_POST['type'];
        if($item->isSkuUnique($_POST['sku']) !== TRUE) {
            $_SESSION['unique_sku'] = "false"; 
            return false;
        } else {
            if(!empty($_SESSION['unique_sku']))
                {
                    unset($_SESSION['unique_sku']);
                }
        }
        $post = array_values($_POST);
        switch($_POST['type']) {
            case "cd":
                $item->insertCd($post);
                break;
            case "furniture":
                $item->insertFurniture($post);
                break;
            case "book":
                $item->insertBook($post);
                break;
        }
    }
}
