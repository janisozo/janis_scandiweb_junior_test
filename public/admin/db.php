<?php
class Database {
    
    /* This class is flawed because i added methods to serve a model - The store "Item"
    But the methods should be implemented in the model themselves */

    // setup vars
    private $db_host = 'localhost';
    private $db_user = 'root';
    private $db_pass = 'password';
    private $db_name = 'scandiweb_test';
    public $connection;

    public function __construct() {
        $this->connection = $this->connect();
    }

    public function connect() {
        $connection = new mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
        // Check conn
        if(mysqli_connect_errno()) {
            echo ("Connect failed: " . mysqli_error($connection) . mysqli_connect_errno());
        }
        return $connection;
    }

    public function prepare($sql) {
        return $stmt = $this->connection->prepare($sql);

    }

    public function prepare_item() {
        $sql = "INSERT INTO items (sku, name, price, type, spec_value) VALUES (?, ?, ?, ?, ?)";
        $stmt = $this->connection->prepare($sql);
        return $stmt;
    }

    public function bind_item_params($stmt, $postRefs) {
        $typeSyms = "ssdss";
        call_user_func_array(array($stmt, "bind_param"), array_merge(array($typeSyms), $postRefs));
        $result = $stmt->execute();
        
        if($result === false) {
            return false;
        } else {
            header("Location: product_list.php");
        }
    }

    public function query($sql) {
        $result = mysqli_query($this->connection, $sql);
        if (!$result) {
            return false;
        }
        return $result;
    }
}
