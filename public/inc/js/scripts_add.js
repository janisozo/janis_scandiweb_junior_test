/* Script for Add page */

/* Vars */
/* Static form inputs */
var skuInputField = document.getElementById('sku-input-field');
var nameInputField = document.getElementById('name-input-field');
var priceInputField = document.getElementById('price-input-field');

/* Dynamic form sections */
var cdInputForm = document.getElementById('cd-form-groups');
var furnitureInputForm = document.getElementById('furniture-form-groups');
var bookInputForm = document.getElementById('book-form-groups');

/* Dynamic form inputs */
var cdInputField = document.getElementById('cd-input');
var heightInputField = document.getElementById('height-input');
var widthInputField = document.getElementById('width-input');
var lengthInputField = document.getElementById('length-input');
var weightInputField = document.getElementById('weight-input');

/* Type Switch Vars */
var typeSwitcher = document.getElementById('type-switcher');
var option = typeSwitcher.options[typeSwitcher.selectedIndex].value;

// Load appropriate form on launch
loadDynamicForm(option);

// Change form content dynamically
typeSwitcher.addEventListener('change', function () {
    option = typeSwitcher.options[typeSwitcher.selectedIndex].value;
    loadDynamicForm(option);
});

/* Display selected type form and clear up inputs */
function loadDynamicForm(option) {
    switch(option) {
        case "cd":
            heightInputField.value = '';
            widthInputField.value = '';
            lengthInputField.value = '';
            weightInputField.value = '';

            cdInputForm.style.display = "block";
            furnitureInputForm.style.display = "none";
            bookInputForm.style.display = "none";
            break;
        case "furniture":
            cdInputField.value = '';
            weightInputField.value = '';

            cdInputForm.style.display = "none";
            furnitureInputForm.style.display = "block";
            bookInputForm.style.display = "none";
            break;
        case "book":
            cdInputField.value = '';
            heightInputField.value = '';
            widthInputField.value = '';
            lengthInputField.value = '';

            cdInputForm.style.display = "none";
            furnitureInputForm.style.display = "none";
            bookInputForm.style.display = "block";
            break;
    }
}

/* Form Validation */
var addForm = document.forms[0];
addForm.addEventListener("click", validateForm);

/* Helper function for decimals */
function countDecimals(value) { 
    if ((value % 1) != 0) 
        return value.toString().split(".")[1].length;  
    return 0;
};

/* Validates all form inputs */
function validateForm(e) {
    var status = true; // If false, form not submitted
    e.preventDefault();

    /* Static field validation */
    if(skuInputField.value === '' || nameInputField.value === '' || priceInputField.value === '' || countDecimals(priceInputField.value) > 2) {
        document.getElementById('static-form-error').removeAttribute('hidden');
        status = false;
    } else {
        document.getElementById('static-form-error').setAttribute('hidden', '');
    }

    /* Dynamic field validation */
    switch(option) {
        case "cd":
            if(cdInputField.value === '' || cdInputField.value < 700 || cdInputField.value % 1 != 0) {
                document.getElementById('error-cd-size').removeAttribute('hidden');
                status = false;
            } else {
                document.getElementById('error-cd-size').setAttribute('hidden', '');
            }
            break;
        case "furniture":
            if(heightInputField.value === '' || countDecimals(heightInputField.value) > 2 
            || widthInputField.value === '' || countDecimals(widthInputField.value) > 2 
            || lengthInputField.value === '' || countDecimals(lengthInputField.value) > 2 ) {                
                document.getElementById('error-furn').removeAttribute('hidden');
                status = false;
            } else {
                document.getElementById('error-furn').setAttribute('hidden', '');
            }
            break;
        case "book":
            if(weightInputField.value === '' || countDecimals(weightInputField.value) > 1) {
                document.getElementById('error-book-weight').removeAttribute('hidden');
                status = false;
            } else {
                document.getElementById('error-book-weight').setAttribute('hidden', '');
            }
    }
    if(status === true) {
        addForm.submit();
    }
}
