var checkInput;
document.addEventListener("DOMContentLoaded", function() {
    // Script for List page
    let formString = /* I inserted this form with javascript because it's hidden input
                     is changed in real-time without explicit AJAX */
        "<form action='" + window.location.href + "' method='post'>" +
            "<select name='select' id='selection' class='mr-4'>" +
                "<option value='delete' selected>Delete</option>" +    
                "<option value='add'>Add a New Item</option>" +   
            "</select>" +
            "<input type='hidden' id='check-input' name='check-input'>" + // This is the checkbox bank
            "<button id='action-button' type='submit'>Apply</button>" +
        "</form>";
    document.getElementById("list-form").innerHTML = formString;

    // Selection for the mass action
    var selection = document.getElementById('selection');
    var selected = selection.options[selection.selectedIndex].value;
    selection.addEventListener('change', function() {
        selected = selection.options[selection.selectedIndex].value;
        /* Selection also works as a link */
        if(selected === "add") {
            window.location.href = "/product_add.php";
        }
    });

    checkInput = document.getElementById('check-input');  // Checkbox bank
});

/* Store item checkbox tracking
 CheckInput is the hidden name field in the form that passes checkbox values to php */
var checkboxes = document.getElementsByClassName('item-checkbox');
var checks = [];
/* Event listeners for checkboxes */
for(let checkbox of checkboxes) {
    checkbox.addEventListener('change', function() {
        if(this.checked){
            checks.push(this.name);
            checkInput.value = checks.toString();
        } else {
            let index = checks.indexOf(this.name);
            checks.splice(index, 1);
            checkInput.value = checks.toString();
        }
    });
}
