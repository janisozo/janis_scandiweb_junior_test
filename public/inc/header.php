<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    session_start();
    include "admin/Item.php"; // Class, which uses DB. Sort of a Model
    include "admin/page_helper.php" // Helper functions for back end. Should be implemented in a Controller
    ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="public/inc/styles/style.css">
    <title>Products For You</title>
</head>
<body>