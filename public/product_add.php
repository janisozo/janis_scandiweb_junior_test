<?php include 'inc/header.php';
checkAdd(); // Check if mass action button was used ?>
    <div class="container">
        <!-- Header Div -->
        <div class="row mb-3 add_header">
            <div class="col-md-12 px-1">
                <h3>Product Add</h3>
                <div class="buttons-floated-right">
                        <a href="product_list.php" class="btn mr-4">Back</a>
                        <form action="product_add.php" method="post" name="addForm">
                            <button id="addFormBtn" type="submit">Save</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
                <!-- Static form content -->
                <div class="col-md-6">
                    <!-- Add Form -->
                    <div class="form-inline-group">
                        <label for="sku">SKU</label>
                        <input type="text" name="sku" id="sku-input-field" required value="<?php if(!empty($_POST['sku'])) { echo $_POST['sku']; }; ?>">
                    </div>
                    <p class="text-danger"
                    <?php if(empty($_SESSION['unique_sku'])) { echo 'hidden'; };
                    unset($_SESSION['unique_sku']);
                    ?>><small>Please enter a unique SKU</small></p>
                    <div class="form-inline-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name-input-field" required value="<?php if(!empty($_POST['name'])) { echo $_POST['name']; }; ?>">
                    </div>
                    <div class="form-inline-group">
                        <label for="price">Price</label>
                        <input type="number" step="0.01" id="price-input-field" name="price" required value="<?php if(!empty($_POST['price'])) { echo $_POST['price']; }; ?>">
                    </div>
                    <p class="text-danger mt-3" id="static-form-error" hidden><small>Please Fill out all the fields properly.</small></p>
                    <div class="form-inline-group">
                        <label for="type" id="type-label">Type Switcher</label>
                        <select name="type" id="type-switcher">
                            <option value="cd" <?php if(!empty($_POST) && $_POST['type'] === 'cd') { echo 'selected';} ?>>CD</option>   
                            <option value="furniture" <?php if(!empty($_POST) && $_POST['type'] === 'furniture') { echo 'selected';} ?>>Furniture</option>
                            <option value="book" <?php if(!empty($_POST) && $_POST['type'] === 'book') { echo 'selected';} ?>>Book</option>
                        </select>
                    </div>
                </div>
                <!-- Dynamic form options -->
                <div class="col-md-5" id="add-form-card">
                    <!-- Content based on type switcher -->
                    <div class='card add-form-dynamic-card'>
                        <!-- CD -->
                        <div id="cd-form-groups">
                            <div class='form-inline-group form-inline-card-group'>
                                <label for='size'>Size</label>
                                <input type='number' name='size' id='cd-input' min="700" value="<?php if($_POST['type'] === 'cd' && !empty($_POST['size'])) { echo $_POST['size']; }; ?>">
                                <p class="text-danger" id="error-cd-size" hidden> Please Provide a valid Memory size.
                                <br> Full numbers only. </p>
                                <p><small>Please enter the Memory size (MB)
                                Min: 700</small></p>
                            </div>
                        </div>
                        <!-- /CD -->
                        <!-- Furn -->
                        <div id="furniture-form-groups">
                            <div class='form-inline-group form-inline-card-group'>
                                <label for='height'>Height</label>
                                <input type='number' step='0.01' name='height' id='height-input'  value="<?php if($_POST['type'] === 'furniture' && !empty($_POST['height'])) { echo $_POST['height']; }; ?>">
                            </div>
                            <div class='form-inline-group form-inline-card-group'>
                                <label for='width'>Width</label>
                                <input type='number' step='0.01' name='width' id='width-input' value="<?php if($_POST['type'] === 'furniture' && !empty($_POST['width'])) { echo $_POST['width']; }; ?>">
                            </div>
                            <div class='form-inline-group form-inline-card-group'>
                                <label for='length'>Length</label>
                                <input type='number' step='0.01' name='length' id='length-input' value="<?php if($_POST['type'] === 'furniture' && !empty($_POST['length'])) { echo $_POST['length']; }; ?>">
                                <p class="text-danger" id="error-furn" hidden> Please Provide Valid Values. Min/Step = 0.01 </p>
                                <p><small>Please enter the Height, Width And Length of the item (Metres)</small></p>
                            </div>
                        </div>
                        <!-- /Furn -->
                        <!-- Book -->
                        <div id="book-form-groups">
                            <div class='form-inline-group form-inline-card-group'>
                                <label for='weight'>Weight</label>
                                <input type='number' name='weight' step="0.1" id='weight-input'  value="<?php if($_POST['type'] === 'book' && !empty($_POST['weight'])) { echo $_POST['weight']; }; ?>">
                                <p class="text-danger" id="error-book-weight" hidden> Please Provide a valid Book Weight. Min/Step = 0.1 </p>
                                <p><small>Please enter the Book weight (KG)</small></p>
                            </div>
                        </div>
                        <!-- /Book -->
                    </div>
                </div>
            </form>
        </div>
    </div>
<script src="public/inc/js/scripts_add.js"></script>
<?php include 'inc/footer.php'; ?>
