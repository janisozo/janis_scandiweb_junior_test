Janis Ozolins

This is my repo for storing the Scandiweb junior test work. 

The app is currently served on my DigitalOcean Droplet at the IP address 167.99.248.53. It is free to manipulate and running as a test.

The 2 main pages are in php, the "admin" folder includes everything to do with db and value manipulation, and there's some heavy form validation in the js folder.

Biggest issues in this project:
	1. There is an "admin" folder in the public folder. It includes DB access and models.
	2. I'm new to routing and htaccess, so the .htaccess file is written as simple as possible.
	3. The DB class has methods that should be transferred to the "Item" model.